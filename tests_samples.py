"""This tests are for some extensions of resources for
https://github.com/django-import-export/django-import-export
"""
from collections import OrderedDict
from django.db.models import BooleanField
from django.test import TestCase
from .. import widgets, exceptions as ie_exceptions
from tablib import Dataset

from ..widgets import BooleanWidget
from ..resources import (CustomFieldsModelResource, ModelResource)
from .io_ext_fake_app.factories import (
    ArtistFactory, MembershipFactory, InstrumentFactory
)
from .io_ext_fake_app.models import Artist, Membership, Instrument
from .io_ext_fake_app.resources import (
    ArtistResourceWithM2M,
    ArtistResourceWithCustomFields,
)


class BaseArtistResource(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.guitar = InstrumentFactory(title="Guitar")

    def setUp(self):
        # create artist
        self.artist = ArtistFactory(instrument=self.guitar)

    def _import_export_artist(self):
        """Return restored Artist """
        dataset = self.resource.export(
            queryset=Artist.objects.filter(id=self.artist.id))

        # delete info about this artist
        self.artist.delete()

        # get new instance of resource
        self.resource = self.resource.__class__()

        # restore info from dataset
        result = self.resource.import_data(dataset)
        return Artist.objects.get(id=result.rows[0].object_id)


class TestResourceWithIntermediateM2M(BaseArtistResource):
    """Test loading of Artist instances using ArtistResource
    """
    def setUp(self):
        super().setUp()
        self.resource = ArtistResourceWithM2M()

        # he played in 3 bands
        MembershipFactory.create_batch(3, artist=self.artist)
        # evaluate queryset to list
        self.bio = list(Membership.objects.all())

        # and there few other artists
        MembershipFactory()
        MembershipFactory()

    def test_correct_restoring(self):
        """This is some kind of functional test for resources.

        Take a look at ``io_ext_fake_app.models``. So we want to export
        ``Artist`` with info about his bands. And we want to store info about
        when artist joined some band in the same flat dataset.

        Test case is following:
            create artist's bio (with joined bands)
            create few other artists
            remove info about original artist
            restore his info from exported dataset
        """
        # retrieve artist from DB
        restored_artist = self._import_export_artist()
        restored_membership = Membership.objects.filter(artist=restored_artist)

        # check that restored artist contain same info
        self.assertEqual(restored_artist.name, self.artist.name)

        # check info about his bands
        self.assertEqual(
            set([membership.band.id for membership in self.bio]),
            set(restored_artist.bands.values_list('id', flat=True))
        )

        self.assertEqual(
            set([membership.date_joined for membership in self.bio]),
            set(restored_membership.values_list('date_joined', flat=True))
        )


class TestCustomFieldsModelResourceMixin(BaseArtistResource):
    """Test loading of Artist instances using
    custom fields model resource mixins
    """

    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        # add few characteristics for Artist's instrument
        # add ``guitar brand`` characteristic
        cls.custom_field = cls.guitar.fields.create(name="Brand")

        # add ``number of strings`` characteristic
        cls.custom_filter = \
            cls.guitar.fields.create_filter(name="Num of strings")

        cls.guitar_five_strings = '5'
        cls.guitar_six_strings = '6'

        cls.custom_filter.choices.create(
            value=cls.guitar_five_strings, color='#00ff00')
        cls.custom_filter.choices.create(
            value=cls.guitar_six_strings, color='#ff0000')

        # set guitar characteristics values
        cls.guitar_brand = "Gibson"

    def setUp(self):
        super().setUp()

        self.resource = ArtistResourceWithCustomFields()
        # create Artist's instrument

        # add characteristics for Artist's instrument
        # add ``guitar brand`` characteristic

        self.artist.custom_fields[self.custom_field] = \
            self.guitar_brand
        self.artist.custom_fields[self.custom_filter] = \
            (self.guitar_five_strings, self.guitar_six_strings)

        self.custom_fields_schema = self.artist.custom_fields.schema

        self.artist.save()

    def test_get_widget(self):
        """Check method '_get_widget' return CharWidget for custom field
        without choices and returns SimpleArrayWidget for custom field
        with choices
        """

        # check CharWidget
        widget = self.resource._get_widget(self.custom_field)
        self.assertEqual(
            widgets.CharWidget,
            widget.__class__
        )

        # check SimpleArrayWidget
        array_widget = self.resource._get_widget(self.custom_filter)
        self.assertEqual(
            widgets.SimpleArrayWidget,
            array_widget.__class__
        )

    def test_custom_field_export_schema_validations(self):
        """Check method '_check_export_schema' is valid """

        # create another artist this same instrument
        ArtistFactory(instrument=self.guitar)

        # check schema for custom_fields is valid
        self.resource._check_export_schema(
            queryset=Artist.objects.all()
        )

        # create artist with another instrument
        ArtistFactory()

        # check schema raise ValueError
        with self.assertRaises(ValueError):
            self.resource._check_export_schema(
                queryset=Artist.objects.all(),
            )

    def test_custom_field_import_schema_validations(self):
        """Check method '_check_export_schema' raise exception if imported
        objects have different schema providers
        """
        # check there are no custom fields schema in resource
        with self.assertRaises(AttributeError):
            getattr(self.resource, 'custom_fields_schema')

        # build custom fields schema
        self.resource._build_custom_fields(self.artist)

        # check that schema was built
        self.assertEqual(
            self.artist.custom_fields.schema,
            self.resource.custom_fields_schema
        )

        # create artist with same instrument and check that there are no
        # exceptions in this method
        self.resource._check_import_schema(
            ArtistFactory(instrument=self.guitar)
        )

        # create artist with another instrument check schema raise ValueError
        with self.assertRaises(ie_exceptions.ImportExportError):
            self.resource._check_import_schema(
                ArtistFactory()
            )

    def test_getting_custom_field_schema(self):
        """Check method '_get_custom_fields_schema' return schema for given
         custom fields attribute name"""

        # get schema by queryset and custom fields attr name
        schema = self.resource._get_custom_fields_schema(
            Artist.objects.all(),
        )

        # check returned schema is the same
        self.assertEqual(
            self.artist.custom_fields.schema,
            schema
        )

    def test_model_has_no_custom_fields(self):
        """Check method '_check_schema_provider' raise exceptions if model
        hasn't custom fields
        """

        # let's create new resource class for model which has't custom fields
        class NewMeta:
            model = Instrument

        resource_dict = OrderedDict(
            Meta=NewMeta,
            use_custom_fields=True
        )

        new_resource = type(
            'new_resource',
            (CustomFieldsModelResource,),
            resource_dict
        )()

        # '_check_schema_provider' must raise ImportExportError because model
        # hasn't custom fields
        with self.assertRaises(ie_exceptions.ImportExportError):
            new_resource._check_schema_provider()

    def test_resource_has_no_schema_provider_field(self):
        """Check method '_check_schema_provider' raise exceptions if custom
        fields provider wasn't declared in resource fields
        """

        # remove schema provider field from resource fields
        self.resource.fields.pop('instrument')

        with self.assertRaises(ie_exceptions.ImportExportError):
            self.resource._check_schema_provider()

    def test_correct_restoring_of_custom_fields(self):
        """Check correct importing/exporting with multiple custom fields
        schemas.
        """
        restored_artist = self._import_export_artist()

        # check instruments custom field `Brand` value
        self.assertEqual(
            restored_artist.custom_fields[self.custom_field],
            self.guitar_brand
        )

        # check instruments custom field `Number of strings` value
        guitar_n_strings = restored_artist.custom_fields[
            self.custom_filter]
        self.assertEqual(
            set(item.value for item in guitar_n_strings),
            {self.guitar_five_strings, self.guitar_six_strings}
        )

    def test_correct_restoring_of_new_instance_with_custom_fields(self):
        """Check correct restoring if imported instance hasn't id value
        """
        # export one artist instance
        dataset = self.resource.export(
            queryset=Artist.objects.filter(id=self.artist.id))

        # pop single row from dataset
        tmp_row = list(dataset.pop())

        # set id param to None value
        tmp_row[0] = None

        # set changed row in dataset again
        dataset.append(tmp_row)

        # delete all artists from db
        Artist.objects.all().delete()

        # reinitialize resource class (drop dynamically created resource
        # custom fields)
        self.resource = ArtistResourceWithCustomFields()

        # restore info from dataset
        self.resource.import_data(dataset)

        # check that data were imported and stored correctly
        self.assertEqual(
            Artist.objects.all().count(),
            1
        )

        # get restored artist
        restored_artist = Artist.objects.all().first()

        # check custom fields schema was restored correctly
        self.assertSetEqual(
            set(restored_artist.custom_fields.schema.values()),
            set(self.custom_fields_schema.values())
        )

        # check instruments custom field `Brand` value restored correctly
        self.assertEqual(
            restored_artist.custom_fields[self.custom_field],
            self.guitar_brand
        )

        # check instruments custom field `Number of strings` value
        # restored correctly
        guitar_n_strings = restored_artist.custom_fields[
            self.custom_filter]
        self.assertEqual(
            set(item.value for item in guitar_n_strings),
            {self.guitar_five_strings, self.guitar_six_strings}
        )

    def test_correct_restoring_few_instances_with_custom_fields(self):
        """Check correct restoring few instances
        """
        # create another artist with same instrument
        guitar_new_brand = 'Cramer'
        another_artist = ArtistFactory(instrument=self.guitar)

        another_artist.custom_fields[self.custom_field] = guitar_new_brand
        another_artist.custom_fields[self.custom_filter] = \
            (self.guitar_five_strings,)

        dataset = self.resource.export(queryset=Artist.objects.all())

        # remove all instances from DB
        Artist.objects.all().delete()

        # reinitialize resource class
        self.resource = ArtistResourceWithCustomFields()
        self.resource.import_data(dataset)

        # check that two instances were restored
        self.assertEqual(
            Artist.objects.all().count(),
            2
        )

    def test_import_export_with_empty_qs(self):
        """Check importing/exporting process with empty queryset
        No one exception shouldn't be raised
        """
        try:
            dataset = self.resource.export(queryset=Artist.objects.none())

            # reinitialize resource class
            self.resource = ArtistResourceWithCustomFields()

            # restore info from dataset
            self.resource.import_data(dataset)

        except Exception as e:
            self.fail("{}".format(e))


class TestResourceClass(TestCase):
    """Tests for ``BaseResource``"""

    def setUp(self):
        class FakeArtistResource(ModelResource):
            class Meta:
                model = Artist
                fields = ('id', 'name', 'instrument')

        self.resource = FakeArtistResource()

    def test_get_boolean_widget(self):
        """Check that ``YesNoBooleanWidget`` returned for Django
        BooleanField
        """
        self.assertEquals(
                self.resource.widget_from_django_field(BooleanField()),
                BooleanWidget
        )

    def test_extra_column_does_not_contain_data(self):
        """Check that method ``after_import_row`` doesn't raise exception if
        extra columns don't contain data
        """
        valid_headers = self.resource.get_export_headers()

        # add some values for all columns
        row = {header: 'some value' for header in valid_headers}

        # set empty value for extra row
        row['Extra row'] = ''

        # check that exception didn't raised
        self.resource.after_import_row(row)

    def test_extra_column_does_contain_data(self):
        """"Check that method ``after_import_row`` raises exception if
        extra columns contain data
        """
        valid_headers = self.resource.get_export_headers()
        valid_headers.append('Extra row')

        # add some values for all columns
        row = {header: 'some value' for header in valid_headers}

        with self.assertRaises(ie_exceptions.ImportExportError):
            self.resource.after_import_row(row)

    def test_import_header_with_extra_spaces_and_different_case(self):
        """Check that exporting document with `dirty` headers is successful

        `dirty` headers are headers with extra whitespaces and any cases
        """
        dataset = Dataset(headers=['id', 'namE', 'InstruMenT'])

        dataset.append((1, 'John', InstrumentFactory().pk))
        dataset.append((2, 'Mike', InstrumentFactory().pk))

        self.resource.import_data(dataset, use_transactions=True)

        artists = Artist.objects.all()

        # check that instance were restored correctly
        self.assertSetEqual(
            set([item.name for item in artists]),
            {'John', 'Mike'}
        )
