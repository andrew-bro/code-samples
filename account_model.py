from django.db import models
from django.utils.translation import ugettext as _
from django.contrib.auth.models import (
    AbstractBaseUser,
    BaseUserManager,
    Group,
    PermissionsMixin
)

from constance import config
from redactor.fields import RedactorField


class AccountRoleEnum(object):
    """
    Simple enum class for account's roles
    """
    AUTHOR = 1
    CORRESPONDENT = 2
    EDITOR = 4

    CHOICES = (
        (AUTHOR, _('Author')),
        (CORRESPONDENT, _('Correspondent')),
        (EDITOR, _('Editor')),
    )

    @classmethod
    def get_choices(cls):
        return [k for k, v in cls.CHOICES]


class AccountManager(BaseUserManager):
    """
    Account manager
    """
    def employees(self):
        """
        Return only employees. Administrators are excluded
        """
        return self.filter(is_active=True)\
            .filter(user_role__in=AccountRoleEnum.CHOICES)

    def correspondents(self):
        return self.filter(is_active=True)\
            .filter(user_role=AccountRoleEnum.CORRESPONDENT)

    def editors(self):
        return self.filter(is_active=True)\
            .filter(user_role=AccountRoleEnum.EDITOR)

    def authors(self):
        return self.filter(is_active=True).filter(
            user_role=AccountRoleEnum.AUTHOR)

    def create_user(self, email=None, password=None, user_role=None):
        if not email:
            raise ValueError(_('Users should have an email address'))

        user = self.model(
            email=self.normalize_email(email),
            user_role=user_role,
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password):
        user = self.create_user(
            email=email,
            password=password,
        )
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)
        return user


class AccountGroupsEnum(object):
    """
    Simple num class for account groups. This groups used for managing
    permissions
    """
    CORRESPONDENTS = _('Correspondents')
    EDITORS = _('Editors')
    CHOICES = [CORRESPONDENTS, EDITORS]


class Account(PermissionsMixin, AbstractBaseUser):
    """
    Model for user's accounts
    """
    email = models.EmailField(
        verbose_name=_('Email'),
        unique=True,
    )
    first_name = models.CharField(
        verbose_name=_('First name'),
        max_length=255
    )
    second_name = models.CharField(
        verbose_name=_('Second name'),
        max_length=255
    )
    position = models.CharField(
        verbose_name=_('Position'),
        max_length=255
    )
    description = RedactorField(
        verbose_name=_('Description'),
        allow_file_upload=True,
        allow_image_upload=True,
        blank=True,
        null=True,
    )
    user_role = models.IntegerField(
        choices=AccountRoleEnum.CHOICES,
        verbose_name=_('Role'),
        blank=True,
        null=True,
    )
    image = models.ImageField(
        verbose_name=_('Photo'),
        upload_to='account/%Y/%m/%d'
    )

    is_active = models.BooleanField(
        verbose_name=_('Is active'),
        default=True,
        null=False
    )
    is_staff = models.BooleanField(
        default=True,
        null=False
    )
    slug = models.SlugField(verbose_name=_('URL'))

    objects = AccountManager()

    USERNAME_FIELD = 'email'

    @property
    def is_author(self):
        """
        Check if user is an author
        Returns:
            bool: yes or no
        """
        return self.user_role == AccountRoleEnum.AUTHOR

    @property
    def is_editor(self):
        """
        Check if user is an editor
        Returns:
            bool: yes or no
        """
        return self.user_role == AccountRoleEnum.EDITOR

    @property
    def is_correspondent(self):
        """
        Check if user is correspondent
        Returns:
            bool: yes or no
        """
        return self.user_role == AccountRoleEnum.CORRESPONDENT

    @property
    def user_group(self):
        """
        Return Django user's `Group` instance for user
        """
        if self.user_role == AccountRoleEnum.CORRESPONDENT:
            return Group.objects.filter(
                name=AccountGroupsEnum.CORRESPONDENTS).first()
        elif self.user_role == AccountRoleEnum.EDITOR:
            return Group.objects.get(name=AccountGroupsEnum.EDITORS).first()

        return Group.objects.none()

    @property
    def last_publications(self):
        """
        Returns few last author's materials

        Returns:
           materials(dict): last news, stories and guides
        """
        limit = config.AUTOR_ARTICLES_LIMIT or 5
        return {
            'news': self.news_set.items().prefetch_related(
                'articlephoto')[:limit],
            'stories': self.stories_set.items().prefetch_related(
                'articlephoto')[:limit],
            'guides': self.guides_set.items().prefetch_related(
                'articlephoto')[:limit],
        }

    def get_short_name(self):
        """
        Returns string representation of user
        """
        return '{0} {1}'.format(self.second_name, self.first_name)

    def get_full_name(self):
        """
        Returns string representation of user
        """
        return self.get_short_name()

    class Meta:
        verbose_name = _('User')
        verbose_name_plural = _('Users')
