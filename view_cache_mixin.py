from django.conf import settings
from django.contrib.auth.models import AnonymousUser
from django.views.decorators.cache import cache_page
from django.utils.http import http_date


class CacheMixin(object):
    """
    This CBV mixin is using for caching pages ony for unregistered visitors of
    web site. Administrators should see page's changes immediately

    """
    def dispatch(self, *args, **kwargs):
        """
        Cache this method if user is anonymous
        """
        if isinstance(self.request.user, AnonymousUser):
            response = cache_page(settings.VIEWS_CACHE_TIMEOUT)(
                super(CacheMixin, self).dispatch)(*args, **kwargs)
        else:
            response = super(CacheMixin, self).dispatch(*args, **kwargs)
        return response

    def render_to_response(self, context, **response_kwargs):
        """
        Add additional header `Last-Modified` to response
        """
        response = super().render_to_response(context, **response_kwargs)

        if hasattr(self, 'get_object'):
            detail_object = self.get_object()
            if hasattr(detail_object, 'updated'):
                response['Last-Modified'] = \
                    http_date(detail_object.updated.timestamp())
        return response
