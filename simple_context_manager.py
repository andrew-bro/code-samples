import os
import requests
import sys
import tempfile

from funcy import last


class download_file(object):
    """
    Simple context manager for downloading file bu url
    """
    def __init__(self, url):
        """
        Args:
            url(str): full url of required file
        """
        self.url = url

    def __enter__(self):
        """
        Download file into local temporary file
        """
        file_ext = last(self.url.split('.'))
        self.tmp_file = tempfile.NamedTemporaryFile(
            delete=False,
            suffix='.{0}'.format(file_ext)
        )

        try:
            r = requests.get(self.url)
            self.tmp_file.write(r.content)
            self.tmp_file.flush()
            self.tmp_file.seek(0)
        except Exception as err:
            self.__exit__(*sys.exc_info())
            raise err

        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        """
        Remove tmp file before exit from context manager
        """
        os.unlink(self.tmp_file.name)
        return True
