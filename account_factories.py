from factory import Faker, DjangoModelFactory, Sequence

from ..models import Account, AccountRoleEnum, AccountGroupsEnum


class BaseAccountFactory(DjangoModelFactory):
    """
    Abstract factory for account model
    """
    first_name = Faker('name')
    second_name = Faker('name')
    position = Faker('word')

    class Meta:
        abstract = True


class UserAccountFactory(BaseAccountFactory):
    """
    Factory for simple user's account
    """
    email = Sequence(lambda n: 'user_%s@bro.agency' % n)

    class Meta:
        model = Account


class CorrespondentAccountFactory(BaseAccountFactory):
    """
    Factory for correspondents
    """
    email = Sequence(lambda n: 'correspondent_%s@bro.agency' % n)
    user_role = AccountRoleEnum.CORRESPONDENT

    class Meta:
        model = Account


class EditorAccountFactory(BaseAccountFactory):
    """
    Factory for editors
    """
    email = Sequence(lambda n: 'editor_%s@bro.agency' % n)
    user_role = AccountRoleEnum.EDITOR

    class Meta:
        model = Account


class AuthorAccountFactory(BaseAccountFactory):
    """
    Factory for users
    """
    email = Sequence(lambda n: 'author_%s@bro.agency' % n)
    user_role = AccountRoleEnum.AUTHOR

    class Meta:
        model = Account


class SuperuserAccountFactory(BaseAccountFactory):
    """
    Factory for superusers
    """
    email = Sequence(lambda n: 'super_%s@bro.agency' % n)
    is_superuser = True

    class Meta:
        model = Account
