"""
This example shows how we can determine material's views per day, per week ...
Sometime also we need to find out what story is more readable. We can store
material's views at the redis DB because this is very fast

Content of file with lua script:

    local app_label = ARGV[1]
    local periods = cjson.decode(ARGV[2])
    local obj_id = ARGV[3]

    for i, period in ipairs(periods) do
        for period_name, period_date in pairs(period) do
            local redis_key = period_date .. "_" .. period_name .. "_" .. app_label
            redis.call('zincrby', redis_key, 1, obj_id)
            redis.call('expire', redis_key, 31*24*60*60)
        end
    end

"""
import json
import os

from datetime import datetime, timedelta
from django_redis import get_redis_connection
from funcy import first


REDIS_PERIOD_KEY_FORMAT = "{date}_{period}_{app_label}"


class RedisArticleEnum(object):
    WEEK = 'week'
    HALF_MONTH = 'half_month'
    MONTH = 'month'

    DAY_IN_SECONDS = 60*60*24

    PERIODS = [
        {
            WEEK: 7,
        },
        {
            HALF_MONTH: 15,
        },
        {
            MONTH: 31,
        }
    ]


def _run_lua_scripts_to_set_drop_from_sorted_sets(object_id, app_label,
                                                  script_name):
    """
    Формируем  набор дат, внутри которых требуется установить просмотры, и
    отправляем их в lua-скрипт
    """
    con = get_redis_connection("default")

    filename = os.path.join(os.path.dirname(__file__),
                            'lua/%s.lua' % script_name)
    with open(filename) as f:
        code = f.read()

    period_names_list = []

    for period in RedisArticleEnum.PERIODS:
        period_name, period_days = first(period.items())
        for day_offset in range(0, period_days):
            future_date = datetime.now() + timedelta(days=day_offset)
            period_names_list.append(
                {period_name: future_date.strftime("%d_%m_%Y")})

    con.register_script(code)(args=[
        app_label,
        json.dumps(period_names_list, default=str),
        object_id,
    ])


def set_redis_views_for_future_days(object_id, app_label):
    """
    If today is 1 january when we should add views of this material for future
    days (week, month...). Because sometimes we would like to retrieve the
    most readable material per month. i.e. if today is 15 February then we
    should get top from 15 Jan to 15 fab
    """
    _run_lua_scripts_to_set_drop_from_sorted_sets(object_id, app_label,
                                                  'set_article_views')


def _get_top_articles(app_label, period_name, limit):
    """
    Get the ids of most readable materials per `period_name`

    Returns:
        ids(list): list of ids
    """
    con = get_redis_connection('default')
    date_str = datetime.now().strftime('%d_%m_%Y')
    redis_key = REDIS_PERIOD_KEY_FORMAT.format(
        date=date_str,
        period=period_name,
        app_label=app_label,
    )
    return [int(i) for i in con.zrevrange(redis_key, 0, limit-1)]


def get_top_articles_per_week(app_label, limit):
    """
    Get ids of most readable materials per week

    Returns:
        ids(list): list of ids
    """
    return _get_top_articles(app_label, RedisArticleEnum.WEEK, limit)
