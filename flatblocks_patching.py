"""
Add additional fields to `FlatBlock` model which belongs to
third-party applicaton. At the our project settings file we should define
local migration path for `flatblocks` app
"""
from django.db import models
from django.utils.translation import ugettext as _
from flatblocks.models import FlatBlock


class FlatBlockTypeEnum(object):
    """
    Simple enum class with choices of flatblock's type
    """
    TEXT = 0
    BANNER = 1

    CHOICES = (
        (TEXT, _('Simple text')),
        (BANNER, _('Banner')),
    )


FlatBlock.add_to_class(
    'type', models.IntegerField(
        verbose_name=_('Type'),
        choices=FlatBlockTypeEnum.CHOICES,
        default=FlatBlockTypeEnum.TEXT,
    )
)


FlatBlock.add_to_class(
    'image', models.FileField(
        upload_to='flatblocks/%Y/%m/%d',
        verbose_name=_('Image'),
        blank=True,
        null=True
    )
)

FlatBlock.add_to_class(
    'url', models.CharField(
        verbose_name='Url',
        max_length=255,
        blank=True, null=True,
    )
)